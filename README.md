# How to setup noosfero 1.4**

From [noosfero website](http://noosfero.org/):
    
    Noosfero is a web platform for social and solidarity economy networks 
    with blog, e-Porfolios, CMS, RSS, thematic discussion, events agenda 
    and collective inteligence for solidarity economy in the same system! 
    Get to know, use it, participate and contribute to this free software
    project!  


**Noosfero 1.4 is only tested in Debian Jessie (Debian 8)

Copy the the text below into a file (ex: noosferosetup.py)

    import os,sys
    
    
    def read_sources():
        f = open("/etc/apt/sources.list")
        r = f.read()
        print(r)
        f.close()
        return r
    
    def write_sources():
        f = open("/etc/apt/sources.list",'a')
        s = '''\n#noosfero
    deb http://download.noosfero.org/debian/jessie-1.4 ./
    deb-src http://download.noosfero.org/debian/jessie-1.4 ./'''
        #print(f.read())
    
        f.write(s)
        f.close()
        
    
    r = read_sources()
    
    if not "noosfero" in r:
        write_sources()
        read_sources()
    
    
    
    os.system('apt update')
    os.system("apt install exim4 postgresql")
    os.system("apt install noosfero noosfero-apache")

Save the script and run in a terminal as root in a Debian 8 (pc/server/virtual machine):

    python noosferosetup.py
    
The script will update the server /etc/apt/source.list, do a apt update.
and install dependencies and start to install noosfero. 

The apt will prompt to allow install noosfero without a key signature. 
Select yes and go on. 

At some point, the noosfero instalation will prompt a dialog about 
database setup.   
Just select Yes (or ok). Then input a password (twice), the domain for 
noosfero, and the installer will warning you that it needs a working   
email setup. Just press enter and go on. 

After it, will populate the database and finish. 

But and now? Now is time to set the admin user:

	noosfero-console
    
the console will be open

	user = User.create(:login => 'adminuser', :email => 'admin@example.com', :password => 'admin', :password_confirmation => 'admin', :environment => Environment.default, :activated_at => Time.new)
    
and then

	user.activate
    
    
This short tutorial only covers the basic setup. 
For more instructions on finish setup, try the pdf  from UNB  
**Warning: The instructions are to a older version in Debian 7**   (in Portuguese) 
<https://fga.unb.br/articles/0000/8080/Guia-Instalacao-Configuracao-Noosfero.pdf>

Documentation on usage of noosfero can be found here:
<http://softwarelivre.org/doc> (in Portuguese)


	

	